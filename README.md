# PID Motor feedback controll

The project integral embedded system NodeMCU ,IoT, NodeJS and MQTT to subscript and publish value to monitor rps speed motor, analog resistor, digital Switch

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
git clone https://gitlab.com/Lawstarkuna/embedded-system.git
```

## Usage

### 1.create your MQTT Could 
### 2.create your Server
### 3.create your Frontend and connect Server via APIs or Socket IO
### 4.connect your MQTT with Server
### 5.connect your NodeMCU with MQTT and publish a value such as rps, analog resistor, digital Switch
### 6.Integral your system and Display it via Frontend

## Any helps

[Ananda](https://www.facebook.com/Aomlawstar)\
[Watcharin](https://www.facebook.com/kazemaru.reborn)\
[Nutdanai](https://www.facebook.com/nut99zamak)
