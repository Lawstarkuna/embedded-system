const express = require('express')
const app = express()
const http = require('http');
const mqtt = require('mqtt')
const bodyParser = require('body-parser')
const cors = require('cors')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(express.static('public'))

var message_rps = "";
var message_regis = "";
var message_switch = "";

options = {
  port: 16953,
  clientId: "MqttjMotorControl",
  username: "lszircbk",
  password: "Qprvc7i7XeHf",
};

const client = mqtt.connect("mqtt://soldier.cloudmqtt.com", options)

client.on('connect', function () {
  client.subscribe('/rps', function (err) {
    if (!err) {
      console.log("mqtt connect rps")
    }
  })
})

client.on('connect', function () {
  client.subscribe('/regis', function (err) {
    if (!err) {
      console.log("mqtt connect regis")
    }
  })
})

client.on('connect', function () {
  client.subscribe('/switch', function (err) {
    if (!err) {
      console.log("mqtt connect switch")
    }
  })
})

client.on('message', function (topic, message, packet){
  // message_rps = message.toString();
  // console.log('Topic=' +  topic + '  Message=' + message);
  if(topic == "/rps"){
    message_rps = message.toString();
    // console.log('Rps = ' +  message_rps);
  }
  else if(topic == "/regis"){
    message_regis = parseInt(message.toString());
    // console.log('Registor = ' +  message_regis);
  }
  else if(topic == "/switch"){
    message_switch = parseInt(message.toString());
    // console.log('Switch = ' +  message_switch);
  }
  
})

app.get('/rps', function(req, res) {
  res.json({"Rps" : message_rps ,"Switch" : message_switch, "Registor" : message_regis})
});

app.get('/registor', function (req, res) {
  res.json({"Registor" : message_regis})
})

app.get('/switch', function (req, res) {
    res.json({"Switch" : message_switch})
  })

app.post('/controlmotor', function (req, res) {
  var direction = req.body.Direction
  var speed = req.body.Speed
  var kp = req.body.Kp
  var ki = req.body.Ki
  var kd = req.body.Kd
  var flag = req.body.Flag
  res.status(200).send({ MSG: 'DIR;' + direction + ';SPEED;' + speed + ';Kp;' + kp + ';Ki;' + ki+ ';Kd;' + kd+ ';Flag;' + flag });
  mqttSender(direction ,speed , kp, ki, kd, flag)
})


function mqttSender(direction ,speed ,kp , ki, kd, flag ) {
  client.on('connect', function () {
    client.subscribe('/motor', function (err) {
      if (!err) {
        console.log("mqtt connect")
      }
    })
  })
  client.publish('/motor', 'DIR;' + direction + ';SPEED;' + speed + ';Kp;' + kp + ';Ki;' + ki+ ';Kd;' + kd+ ';Flag;' + flag )
}

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  next();
});

http.createServer(app).listen(80, () => {
  console.log('HTTP Server running on port 80');
});