#include<ESP8266WiFi.h>
#include<PubSubClient.h>
#include <ESP8266HTTPClient.h>
#include <timer.h>
#include <EEPROM.h>

auto timer = timer_create_default();

#define ssid "xuselab"
#define password "012345678"

#define IN1 D0
#define IN2 D1
#define EN D3
#define OPA D2
#define Switch D4

#define mqttServer "soldier.cloudmqtt.com"
#define mqttPort 16953
#define mqtt_user "lszircbk"
#define mqtt_password "Qprvc7i7XeHf"

unsigned long prev_dt;
double elapsed_dt;

bool SwitchFlag = false;

String msg = "";
char msgchar[50];
char rpschar[50];
String direction = "";
String speed = "";
double SpeedControl = 0;


double rps = 0;
double counter = 0;
int currentState = 0;
int previousState = 0;
byte err_check_every2_second = 0;

int inputRps = 0;
double previous_error = 0;
double integral = 0;
double kp = 1;
double ki = 0.5;
double kd = 0.5;

int Value_P_address = 0;
int Value_I_address = 5;
int Value_D_address = 10;

int val = 0;

String value_P = "1";
String value_I = "0.5";
String value_D = "0.5";
long int lastSpark;

bool isSetupComplete = false;

String EEPROM_read(int index, int length)
{
  String text = "";
  char ch = 1;

  for (int i = index; (i < (index + length)) && ch; ++i)
  {
    if (ch = EEPROM.read(i))
    {
      text.concat(ch);
    }
  }
  return text;
}

int EEPROM_write(int index, String text)
{
  for (int i = index; i < text.length() + index; ++i)
  {
    EEPROM.write(i, text[i - index]);
  }
  EEPROM.write(index + text.length(), 0);
  EEPROM.commit();

  return text.length() + 1;
}

int conf_EEPROM_write(int function, String value){
  int len = EEPROM_write(function, value);
  return len;
}

WiFiClient espClient;
PubSubClient client(espClient);


void setup(){
  Serial.begin(115200);

  Serial.println();
  Serial.println("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid,password);

  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");  
  }


  
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);


  

  
  EEPROM.begin(512);
  
  int len = 15;
//  len = conf_EEPROM_write(Value_P_address,value_P);
  value_P = EEPROM_read(Value_P_address, len);
//  Serial.print("Value P : ");
//  Serial.println(value_P);

//  len = conf_EEPROM_write(Value_I_address,value_I);
  value_I = EEPROM_read(Value_I_address, len);
//  Serial.print("Value I : ");
//  Serial.println(value_I);
  
//  len = conf_EEPROM_write(Value_D_address,value_D);
  value_D = EEPROM_read(Value_D_address, len);
//  Serial.print("Value D : ");
//  Serial.println(value_D);

  kp = (double)value_P.toFloat();
  Serial.print("kp : ");
  Serial.println(kp, 2);
  ki = (double)value_I.toFloat();
  Serial.print("ki : ");
  Serial.println(ki, 2);
  kd = (double)value_D.toFloat();
  Serial.print("kd : ");
  Serial.println(kd, 2);
  
  pinMode(OPA,INPUT);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(EN,OUTPUT);
  pinMode(Switch,INPUT);

   attachInterrupt(digitalPinToInterrupt(OPA), calRps, FALLING);
   controlMotor();
    timer.every(1000, motorCount);
}

void loop() {
  
    while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect("ESP8266Client", mqtt_user, mqtt_password)){
      Serial.println("connected");
      client.subscribe("/motor");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
//      return;
    }
  }
  client.loop();
  
  
//  calRps();
  check_switch();

//
  if (val == 1 && (micros() - lastSpark >= 3200) ) {
    currentState = 1;
  }
  else {
    currentState = 0;
  }
  
  if(currentState != previousState){
    if(currentState == 1){
      counter = counter + 1.;  
      
    }
    val = 0;
  }
  previousState = currentState;

  timer.tick();
}

void calRps(){
//  counter++;
  val = 1;
  lastSpark = micros();

}

void callback(char* topic, byte* payload, unsigned int length) {
  //control motor :DIR;LEFT;SPEED;50;Kp;none;Ki;none;Kd;none;Flag;0;
  // Write EEPROM :DIR;none;SPEED;none;Kp;1;Ki;1;Kd;0.5;Flag;1;
  int i=0;
  while (i<length) msg += (char)payload[i++];
    msg.toCharArray(msgchar, 50);
    char *p = msgchar;
    char *str;
    byte k = 0;
    direction = "";
    speed = "";
    String kp_str = "";
    String ki_str = "";
    String kd_str = "";
    String Flag_str = "";
    while ((str = strtok_r(p, ";", &p)) != NULL) // delimiter is the semicolon
    {
      if(k==1)
      {
        direction = str;
      }
      if(k==3)
      {
        speed = str;
      }
      if(k==5)
      {
        kp_str = str;
      }
      if(k==7)
      {
        ki_str = str;
      }
      if(k==9)
      {
        kd_str = str;
      }
      if(k==11)
      {
        Flag_str = str;
      }
      k=k+1;
    }

    if(Flag_str == "1")
    {
      int lenx = 15;
      lenx = conf_EEPROM_write(Value_P_address,kp_str);
      value_P = EEPROM_read(Value_P_address, lenx);

      lenx = conf_EEPROM_write(Value_I_address,ki_str);
      value_I = EEPROM_read(Value_I_address, lenx);
  
      lenx = conf_EEPROM_write(Value_D_address,kd_str);
      value_D = EEPROM_read(Value_D_address, lenx);

      kp = (double)value_P.toFloat();
      Serial.print("kp change : ");
      Serial.println(kp, 2);
      ki = (double)value_I.toFloat();
      Serial.print("ki change: ");
      Serial.println(ki, 2);
      kd = (double)value_D.toFloat();
      Serial.print("kd change: ");
      Serial.println(kd, 2);
      Serial.println("");
    }
    else{
    
      inputRps = speed.toInt();
      limitMotor();
      controlMotor();
      msg = "";
    }
}

void limitMotor(){
      if(SpeedControl < 0)
      {
        SpeedControl = 0;
      }
     else if (SpeedControl > 1023)
      {
        SpeedControl = 1023;
      }
      analogWrite(EN,SpeedControl);
}



bool motorCount(void *){
  
  
  algo(counter, false);
  
//here to publish to topic 

  counter = 0;
//  Serial.println("\n");
  sendRegister();
  return true;
}

void algo(int count, bool isSetup){

  err_check_every2_second++;
 
  if((err_check_every2_second %2 == 0) || isSetup){
      rps = counter/2;
      double error_value = inputRps - rps;
      
    // this comment for Serial graph plot
//    Serial.print(inputRps);
//    Serial.print("\t");
//    Serial.println(rps); 
 
     Serial.println("");
     Serial.print(" | CURRENT PWM: ");
     Serial.print(SpeedControl);
     Serial.print(" | Rps : ");
     Serial.print(rps);   
     Serial.print(" | Error_value: ");
     Serial.print(error_value);

      String str = String(rps);
      str.toCharArray(rpschar, 50);
      client.publish("/rps",rpschar);
      
    if(abs(error_value) >= 2){

    elapsed_dt = 2;
    integral = integral + (double)(error_value*elapsed_dt);
    double derivative = (double)(error_value - previous_error)/(double)elapsed_dt;
    SpeedControl = SpeedControl + (kp*error_value) + (ki*integral) + (kd*derivative);
    
    limitMotor();
    
    }
    else{
      
      err_check_every2_second = 0;
      integral = 0;
    }
    previous_error = error_value;


  }
}
void controlMotor(){
  if (direction == "RIGHT")
  {
    analogWrite(EN,SpeedControl);
    digitalWrite(IN1,HIGH);
    digitalWrite(IN2,LOW);
  }
  else if(direction == "LEFT"){
    analogWrite(EN,SpeedControl);
    digitalWrite(IN1,LOW);
    digitalWrite(IN2,HIGH);
    }
  else if(direction == "STOP"){
    SpeedControl = 0;
    digitalWrite(IN1,LOW);
    digitalWrite(IN2,LOW);
    }
}
void check_switch(){
    int SwitchStatus = digitalRead(Switch);
  if (SwitchStatus == 1 && SwitchFlag == false) {
    client.publish("/switch","0");
    SwitchFlag = true;
  } else if (SwitchStatus == 0 && SwitchFlag == true) {
    client.publish("/switch","1");
    SwitchFlag = false;
  }
delay(1);
}
void sendRegister()
{
    char val_char[50];
    int val = analogRead(A0);
    String val_str = String(val);
    val_str.toCharArray(val_char, 50);
    client.publish("/regis",val_char);

}
